# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app
COPY ./requirements.txt ./requirements.txt
WORKDIR /app/src
COPY ./src/predict_app.py ./predict_app.py
WORKDIR /app/params
COPY ./params/predict_app.yml ./predict_app.yml
WORKDIR /app/models
COPY ./models/model_1.joblib ./model_1.joblib

WORKDIR /app
RUN pip3 install -r requirements.txt
CMD ["python3", "./src/predict_app.py"]