import numpy as np
import pandas as pd
from joblib import dump
from sklearn.linear_model import LinearRegression
import click
from datetime import datetime
import yaml


@click.command()
@click.option('--data_path', default='data/processed/train.csv')
@click.option('--model_path', default='models/model_1.joblib')
@click.option('--config', default='params/train.yml')
def train(data_path, model_path, config):
    train_df = pd.read_csv(data_path)
    X = train_df['total_meters'].to_numpy().reshape(-1, 1)
    y = train_df['price']
    model = LinearRegression().fit(X, y)
    make_train_report(X, config, model, y)
    dump(model, model_path)


def make_train_report(X, config, model, y):
    score = model.score(X, y)
    k, b = model.coef_[0], model.intercept_
    report = [
        f'Training date time: {datetime.now()}\n',
        f'Training data len: {len(X)}\n',
        f'R^2 is {score:.4f}\n',
        f'Price = ({int(k)} * meters + {int(b)}) * 1000 RUB'
    ]
    with open(config) as file:
        config = yaml.safe_load(file)
    with open(config['report_path'], 'w', encoding='utf-8') as file:
        file.writelines(report)


if __name__ == '__main__':
    train()
