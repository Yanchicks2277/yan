import click
import pandas as pd
import yaml
from sklearn.model_selection import train_test_split


def foots_to_meters(foots: int) -> int:
    return round(foots * 0.092903)


def dollars_to_rubles(dollars: int) -> int:
    return round(dollars * 82 / 1000)


@click.command()
@click.option('--in_data', default='data/raw/kaggle/train.csv')
@click.option('--out_data', default='data/processed/train.csv')
@click.option('--config', default='params/preprocess_kaggle.yml')
def preprocess_data(in_data, out_data, config):
    with open(config) as file:
        config = yaml.safe_load(file)
    df = pd.read_csv(in_data)
    new_df = df[['GrLivArea', 'SalePrice']]

    new_df.loc[:, 'GrLivArea'] = new_df['GrLivArea'].map(foots_to_meters)
    new_df.loc[:, 'SalePrice'] = new_df['SalePrice'].map(dollars_to_rubles)
    new_df = new_df.rename(columns={'GrLivArea': 'total_meters', 'SalePrice':'price'})
    train_df, test_df = train_test_split(new_df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])


if __name__ == '__main__':
    preprocess_data()
