import boto3
from dotenv import load_dotenv

bucket_name = 'pabdsample'
load_dotenv(".env")  # записывает в переменные среды

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

# Получить список объектов в бакете
for key in s3.list_objects(Bucket=bucket_name)['Contents']:
    print(key['Key'])

## Из файла
# s3.upload_file('models/model_1.joblib', bucket_name, 'model_221694.joblib')
